<?php 
/* Veritabani bilgilerine kesinlikle root hesabinin bilgilerini girmeyiniz.
Bu, guvenliginiz icin buyuk bir risk olusturabilir.

Author : Ahmet Ağar a.k.a @arguewithme

Bu script uye nosu girilen profildeki tum profil mesajlarini temizler.
*/
$dbHost     = 'localhost';
$dbUsername = mysql_real_escape_string($_POST['dbUsername']);
$dbPassword = mysql_real_escape_string($_POST['dbPassword']);
$dbName     = mysql_real_escape_string($_POST['dbName']);
$userID 	= mysql_real_escape_string($_POST['userID']);
$dbBaglantisi = mysqli_connect($dbHost, $dbUsername, $dbPassword) or die(mysqli_connect_error($dbBaglantisi));
mysqli_select_db($dbBaglantisi, $dbName) or die("Veritabani Secilemedi");
mysqli_query($dbBaglantisi, "DELETE FROM $dbName.visitormessage WHERE visitormessage.userid = $userID");
mysqli_query($dbBaglantisi, "DELETE FROM $dbName.visitormessage_hash WHERE visitormessage_hash.userid = $userID");
if (mysqli_affected_rows($dbBaglantisi) > 0) {
	echo "Etkilenen satir sayisi: " .mysqli_affected_rows($dbBaglantisi);
}
else {
	echo "Hicbir satir etkilenmedi.";
}
mysqli_close($dbBaglantisi);
?>